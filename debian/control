Source: libjung-free-java
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Michael R. Crusoe <crusoe@ucdavis.edu>,
           Olivier Sallou <osallou@debian.org>,
           Pierre Gruet <pgt@debian.org>
Section: java
Priority: optional
Build-Depends: debhelper-compat (= 13),
               default-jdk,
               javahelper,
               libguava-java,
               libmaven-javadoc-plugin-java,
               libmaven-dependency-plugin-java,
               maven-debian-helper,
               junit4 <!nocheck>
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/med-team/libjung-free-java
Vcs-Git: https://salsa.debian.org/med-team/libjung-free-java.git
Homepage: https://github.com/jrtom/jung
Rules-Requires-Root: no

Package: libjung-free-java
Architecture: all
Depends: ${maven:Depends},
         ${misc:Depends}
Recommends: ${maven:Recommends}
Description: Java Universal Network/Graph Framework
 JUNG provides a common and extendible language for the modeling, analysis, and
 visualization of data that can be represented as a graph or network.
 .
 This package does not contain the jung jai classes (Java Advanced Imaging) for
 licensing reasons.
 .
 The JUNG architecture is designed to support a variety of representations of
 entities and their relations, such as directed and undirected graphs,
 multi-modal graphs, graphs with parallel edges, and hypergraphs. It provides
 a mechanism for annotating graphs, entities, and relations with metadata. This
 facilitates the creation of analytic tools for complex data sets that can
 examine the relations between entities as well as the metadata attached to each
 entity and relation.
 .
 The current distribution of JUNG includes implementations of a number of
 algorithms from graph theory, data mining, and social network analysis, such as
 routines for clustering, decomposition, optimization, random graph generation,
 statistical analysis, and calculation of network distances, flows, and
 importance measures (centrality, PageRank, HITS, etc.).
 .
 JUNG also provides a visualization framework that makes it easy to construct
 tools for the interactive exploration of network data. Users can use one of the
 layout algorithms provided, or use the framework to create their own custom
 layouts. In addition, filtering mechanisms are provided which allow users to
 focus their attention, or their algorithms, on specific portions of the graph.

Package: libjung-free-java-doc
Architecture: all
Section: doc
Depends: ${maven:DocDepends},
         ${misc:Depends}
Recommends: ${java:Recommends}
Multi-Arch: foreign
Description: Java Universal Network/Graph Framework (documentation)
 JUNG provides a common and extendible language for the modeling, analysis, and
 visualization of data that can be represented as a graph or network.
 .
 The JUNG architecture is designed to support a variety of representations of
 entities and their relations, such as directed and undirected graphs,
 multi-modal graphs, graphs with parallel edges, and hypergraphs. It provides
 a mechanism for annotating graphs, entities, and relations with metadata. This
 facilitates the creation of analytic tools for complex data sets that can
 examine the relations between entities as well as the metadata attached to each
 entity and relation.
 .
 The current distribution of JUNG includes implementations of a number of
 algorithms from graph theory, data mining, and social network analysis, such as
 routines for clustering, decomposition, optimization, random graph generation,
 statistical analysis, and calculation of network distances, flows, and
 importance measures (centrality, PageRank, HITS, etc.).
 .
 JUNG also provides a visualization framework that makes it easy to construct
 tools for the interactive exploration of network data. Users can use one of the
 layout algorithms provided, or use the framework to create their own custom
 layouts. In addition, filtering mechanisms are provided which allow users to
 focus their attention, or their algorithms, on specific portions of the graph.
 .
 This package contains the documentation.
